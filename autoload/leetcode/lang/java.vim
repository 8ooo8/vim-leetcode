let s:lang = 'java'
let s:ext = 'java'

fu! leetcode#lang#java#locateLeetcodeCliComment(topmost_line)
endfu

fu! leetcode#lang#java#getCustomDependencies()
endfu

fu! leetcode#lang#java#addDependencies()
endfu

fu! leetcode#lang#java#foldDependencies()
endfu

fu! leetcode#lang#java#goToWhereCodeBegins()
endfu

fu! leetcode#lang#java#commentDependencies()
endfu

fu! leetcode#lang#java#uncommentDependencies()
endfu

fu! leetcode#lang#java#getExt()
  retu s:ext
endfu
